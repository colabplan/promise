
#SCSSの構造について
 

## _cmn_scss　グローバル設定ファイル
PCと共有しているグローバル設定ファイルが以下にあります。

`src/develop/Pcmain/_cmn_scss/`

- reset.css、base.css
- アイコンフォントの設定
- 変数、mixinの設定
- 共通モジュール

といった内容になります。

ベースを組んでしまっているので、特に追加・変更の必要はないとは思いますが、
構造把握の際には参照くださいませ。


***
## _sp_scss　SP用のSCSSファイル
SP用のSCSSファイルは以下にまとまっています。

`src/develop/Pcmain/_sp_scss/`


> **_common_modules.scss**	
> ⇒ 会員サービス / ログイン前共通	

~	

> **_all_common_modules.scss**		
> ⇒ ログイン前用commonファイル

~	

> **_form.scss**	
> ⇒ フォーム用のcssファイル

~	

> **_individualフォルダ**	
> ⇒ カテゴリ別のcssファイル

***
####_individualフォルダ内訳	

以下が内訳です。	
基本的にはindividual内のscssのみを編集してくださいませ。
SPでは未作成のファイルもありますが、PCのルールに則っています。
scss未作成のカテゴリに着手する場合は以下に則って新規作成してくださいませ。

>> **_CPA00.scss**	
>> ⇒ [会員サービスTOP](http://promise.test-sitecheck.com/Pcmain/CPA00Control/CPA00011_sp.html)

~	

>>  **_APA00.scss**		
>>  ⇒ [ログイン前TOP](http://promise.test-sitecheck.com/Pcmain/APA00Control/APA00001_sp.html)


***

####ログイン前	
>>  **_firsttime.scss
>>  ⇒ [はじめてのご利用](http://promise.test-sitecheck.com/Pcmain/APD61Control/APD61001_sp.html)カテゴリ

~	
>>  **_loan.scss**	
>>  ⇒ [お借入れ](http://promise.test-sitecheck.com/Pcmain/APD63Control/APD63002_sp.html)カテゴリ

~	
>> **_repayment.scss**		
>>  ⇒ [ご返済](http://promise.test-sitecheck.com/Pcmain/APD64Control/APD64002_sp.html)カテゴリ

~	
>>  **_login.scss**		
>> ⇒ [会員ログイン](http://promise.test-sitecheck.com/Pcmain/CPA00Control/CPA00001_sp.html)

~	
>>  **_popup.scss**		
>> ⇒ [ポップアップ](http://promise.test-sitecheck.com/Pcmain/APD80Control/APD80001_sp.html)など

~	
>> **_availability.scss**	
>> ⇒ [会員ページでできること](http://promise.test-sitecheck.com/Pcmain/APD73Control/APD73001_sp.html)カテゴリ

~	
>> **_service.scss**	
>> ⇒ [商品・サービスのご紹介](http://promise.test-sitecheck.com/Pcmain/APD67Control/APD67001_sp.html)カテゴリ

~	
>> **_ladies.scss**		
>>  ⇒ [レディースキャッシング](http://promise.test-sitecheck.com/Pcmain/APA00Control/APA00008_sp.html)

~	
>>  **_shop.scss**	
>> ⇒ [店舗・ATM](http://promise.test-sitecheck.com/Pcmain/APD80Control/APD80001_sp.html)

~	
>>  **_other.scss**		
>>  ⇒ その他ページ

***
####会員サービス配下
>>  **_member_app.scss**	