			<nav id="local_navigation">
				<p class="title"><a href="../APD68Control/APD68001.html">よくあるご質問・<br>お問い合わせ</a></p>
				<ul>
					<li data-link="APD68002"><a href="../APD68Control/APD68002.html"><span class="arrow arw01r">お申込みについて</span></a></li>
					<li data-link="APD68003"><a href="../APD68Control/APD68003.html"><span class="arrow arw01r">キャッシングについて</span></a></li>
					<li data-link="APD68004"><a href="../APD68Control/APD68004.html"><span class="arrow arw01r">ご返済について</span></a></li>
					<li data-link="APD68007"><a href="../APD68Control/APD68007.html"><span class="arrow arw01r">改正貸金業法について</span></a></li>
					<li data-link="APD68008"><a href="../APD68Control/APD68008.html"><span class="arrow arw01r">スマートフォンアプリについて</span></a></li>
					<li data-link="APD68005"><a href="../APD68Control/APD68005.html"><span class="arrow arw01r">その他のご質問</span></a></li>
					<li data-link="BPD00001"><a href="../BPD00Control/BPD00001.html"><span class="arrow arw01r">お問い合わせ</span></a></li>
				</ul>
			</nav>