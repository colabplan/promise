			<nav class="nav_box">
				<ul class="btn_list">
					<li>
						<p class="btn type01 large"><a href="/Pcmain/BPA00Control/BPA00001.html" class="ico_pen02 text_color_white"><span class="small">24時間いつでもOK!</span>新規お申込み</a></p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/BPB00Control/BPB00001.html">お借入シミュレーション</a></p>
						<p class="btm_text">借りられるかな？</p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/BPB01Control/BPB01001.html">ご返済シミュレーション</a></p>
						<p class="btm_text">返せるかな？</p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/APD61Control/APD61015.html">お借入プラン診断</a></p>
						<p class="btm_text">どうやって返すの？</p>
					</li>
					<li>
						<p class="btn type01 white no_grad"><a href="/Pcmain/APE00/APE00129.html">はじめてのご利用で<br>30日間無利息</a></p>
					</li>
				</ul>
			</nav>