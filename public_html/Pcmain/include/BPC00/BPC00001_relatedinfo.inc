<div id="footer_contentslink" class="fl_content">
		<div class="inner">
			<div class="fl_left box">
				<h5 class="ico_qt02">よくあるご質問</h5>
				<ul>
					<li class="arrow arw01r"><a href="#">申込みはどこでできるの？</a></li>
					<li class="arrow arw01r"><a href="#">誰にも会わずに契約したいのですが・・・</a></li>
					<li class="arrow arw01r"><a href="#">店舗はどこにあるのですか？</a></li>
					<li class="arrow arw01r"><a href="#">年齢制限はありますか？</a></li>
					<li class="arrow arw01r"><a href="#">アルバイトやパート・派遣社員でも契約できますか？</a></li>
				</ul>
				<p class="gotoindex"><span class="arrow arw01r"><a href="#">よくあるご質問一覧へ</a></span></p>
			</div>
			<div class="fl_left box">
				<h5 class="ico_star01">おすすめコンテンツ</h5>
				<ul>
					<li class="arrow arw01r"><a href="http://promise.tribeck.com/Pcmain/APD61Control/APD61001.html">はじめてのご利用</a></li>
					<li class="arrow arw01r"><a href="http://promise.tribeck.com/Pcmain/APD61Control/APD61002.html">お申込み条件と必要なもの</a></li>
					<li class="arrow arw01r"><a href="http://promise.tribeck.com/Pcmain/APD61Control/APD61003.html">お申込み方法</a></li>
					<li class="arrow arw01r"><a href="http://promise.tribeck.com/Pcmain/APA00Control/APA00008.html">レディースキャッシング</a></li>
				</ul>
			</div>
		</div>
	</div>