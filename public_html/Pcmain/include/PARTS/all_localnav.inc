			<nav id="local_navigation">
				<p class="title"><a href="../APX00PARTS/PARTSLIST.html">ログイン前サイト用パーツ集</a></p>
				<ul>
					<li data-link="GENERAL"><a href="../APX00PARTS/GENERAL.html"><span class="arrow arw01r">通常ページ汎用パーツ集</span></a></li>
					<li data-link="TEXTIMAGE"><a href="../APX00PARTS/TEXTIMAGE.html"><span class="arrow arw01r">テキスト・画像パターン集</span></a></li>
					<li data-link="TABLE"><a href="../APX00PARTS/TABLE.html"><span class="arrow arw01r">テーブルパターン集</span></a></li>
					<li data-link="UTILITY"><a href="../APX00PARTS/UTILITY.html"><span class="arrow arw01r">ユーティリティ集</span></a></li>
					<li data-link="POPUP"><a href="../APX00PARTS/POPUP.html" class="new_window"><span class="arrow arw01r external">ポップアップウィンドウ</span></a></li>
					<li data-link="FIRSTTIME"><a href="../APX00PARTS/FIRSTTIME.html"><span class="arrow arw01r">はじめてのご利用</span></a></li>
					<li data-link="LOAN"><a href="../APX00PARTS/LOAN.html"><span class="arrow arw01r">お借入れ</span></a></li>
					<li data-link="REPAYMENT"><a href="../APX00PARTS/REPAYMENT.html"><span class="arrow arw01r">ご返済</span></a></li>
					<li data-link="LOGIN"><a href="../APX00PARTS/LOGIN.html"><span class="arrow arw01r">新規お申込み/会員ログイン</span></a></li>
					<li data-link="AVAILABILITY"><a href="../APX00PARTS/AVAILABILITY.html"><span class="arrow arw01r">会員ページでできること</span></a></li>
					<li data-link="CONTACT"><a href="../APX00PARTS/CONTACT.html"><span class="arrow arw01r">よくあるご質問・お問い合わせ</span></a></li>
					<li data-link="SERVICE"><a href="../APX00PARTS/SERVICE.html"><span class="arrow arw01r">商品・サービスのご紹介</span></a></li>
					<li data-link="OTHER"><a href="../APX00PARTS/OTHER.html"><span class="arrow arw01r">その他</span></a></li>

				</ul>
			</nav>