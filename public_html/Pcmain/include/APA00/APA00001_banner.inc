	<div id="footer_bnr">
		<div class="inner fl_content">		
			<p class="text_right">プロミスを展開するＳＭＢＣコンシューマーファイナンスはＳＭＦＧグループの一員です。</p>
			<div class="logo fl_content">
				<a href="http://www.smfg.co.jp/" class="fl_left" target="_blank"><img src="/Pcmain/common/co_smfg_logo.png" alt="三井住友フィナンシャルグループ" ></a>
				<a href="http://www.smbc-cf.com/corporate/" class="fl_right" target="_blank"><img src="/Pcmain/common/co_smbc_logo.png" alt="SMBCコンシューマーファイナンス" ></a>
			</div>
			<div class="bnr">
				<a href="http://www.j-fsa.or.jp/personal/diagnosis/" target="_blank"><img src="/Pcmain/common/co_footer_bnr_kakeishindan.png"  alt="あなたの家計の健全性、改めて見直してみませんか!?&nbsp;&nbsp;日本貸金業協会&nbsp;&nbsp;消費行動診断&nbsp;家計管理診断"></a>
				<a href="http://www.0570-051-051.jp/" target="_blank"><img src="/Pcmain/common/co_footer_bnr_kashikin.png"  alt="貸金業法について&nbsp;日本貸金業協会"></a>			
				<a href="https://login.japannetbank.co.jp/cgi-bin/NBPFAF01?__gid=0000&amp;__type=0003&amp;__sid=00000&amp;__fid=NBGAF120&amp;B_ID=1&amp;__uid=0000&amp;SikibetuId=2014000070" target="_blank"><img src="/Pcmain/common/co_footer_bnr_jnb_visadebitcard.png"  alt="入会審査がないVisaデビットカード&nbsp;<年会費&nbsp;永年無料>ジャパンネット銀行"></a>
				<a href="http://rokkon.jp/" target="_blank"><img src="/Pcmain/common/co_footer_bnr_rokonsai.jpg"  alt="東北六魂祭&nbsp;私たちは東北六魂祭を応援しています。"></a>
			</div>
			<p>プロミスを展開するＳＭＢＣコンシューマーファイナンスは以下の指定信用情報機関に加盟しています。<br>
				<a href="http://www.jicc.co.jp/" target="_blank">（株）日本信用情報機構（JICC）</a>
				<a href="http://www.cic.co.jp/" target="_blank">（株）シー・アイ・シー（CIC）</a>
			</p>
		</div>
	</div>