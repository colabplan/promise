			<nav id="local_navigation">
				<p class="title"><a href="../APD73Control/APD73001.html">会員ページでできること</a></p>
				<ul>
					<li data-link="APD73006"><a href="../APD73Control/APD73006.html"><span class="arrow arw01r">瞬フリ（振込キャッシング）</span></a></li>
					<li data-link="APD73004"><a href="../APD73Control/APD73004.html"><span class="arrow arw01r">インターネット返済</span></a></li>
					<li data-link="APD73007"><a href="../APD73Control/APD73007.html"><span class="arrow arw01r">ポイントサービス</span></a></li>
					<li data-link="APD73002"><a href="../APD73Control/APD73002.html"><span class="arrow arw01r">ご利用限度額変更申込</span></a></li>
					<li data-link="APD73003"><a href="../APD73Control/APD73003.html"><span class="arrow arw01r">口フリ（口座振替）</span></a></li>
					<li data-link="APD73005"><a href="../APD73Control/APD73005.html"><span class="arrow arw01r">ご返済日お知らせメール</span></a></li>
				</ul>
			</nav>