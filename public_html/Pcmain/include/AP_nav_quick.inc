<div id="quick">
	<a href="javascript:void(0)" class="accd_btn type01 open" rel="quick_accd">クイックアクセス</a>
	<div class="accd_contents fl_content" rel="quick_accd" style="display:block">
		<a href="#first_modal" class="open_modal">はじめての<br>お客さまへ</a>
		<a href="#inuse_modal" class="open_modal">ご利用中の<br>お客さまへ</a>
		<a href="#consultation_modal" class="open_modal">ご相談の<br>お客さまへ</a>
	</div>
</div>

<div id="quick_modal_layer" class="modal_item modal_layer">
      <div class="modal_wrap">

	<div id="consultation_modal" class="quick_modal modal_cont"  style="height:420px;">            	
			<h3 class="modal_title">クイックアクセス　ご相談のお客さまへ</h3>
			<a class="close_btn close_btn_box ico_close01" href="javascript:void(0);">close</a>
			<div class="quick_modal_inner">
				<div class="fl_content upper mt25">
				
					<p class="basic_title text_bold fl_left">お客さまからよく寄せられる質問をまとめています。<br>ご質問が見つからない場合はお気軽にお問い合わせください。</p>
					<div class="btn_list fl_right">
								<p class="btn type01 lightblue no_grad single middle"><a href="../BPD01Control/BPD01001.html">お問い合わせはこちら</a></p>
								<p class="btm_text text_center">よくあるご質問に見つからない場合</p>
					</div>
				</div>

				<ul class="btm btn_list fl_content">
							<li>
								<p class="btn type01 lightblue no_grad single middle"><a href="../APD68Control/APD68001.html">よくあるご質問</a></p>
								<p class="btm_text">お問い合わせの多いご質問</p>
							</li>
							<li class="mb_none">
								<p class="btn type01 white no_grad"><a href="../APD68Control/APD68002.html">お申込みに<br>ついて</a></p>
							</li>
							<li class="mb_none">
								<p class="btn type01 white no_grad"><a href="../APD68Control/APD68003.html">キャッシングに<br>ついて</a></p>
							</li>
							<li class="mb_none">
								<p class="btn type01 white no_grad"><a href="../APD68Control/APD68004.html">ご返済に<br>ついて</a></p>
								<p class="btm_text"><span class="arrow arw01r"><a href="../APD68Control/APD68005.html">その他のご質問はこちら</a></span></p>

							</li>
				</ul>
			</div>
		</div><!--#consultation_modal -->
		

		<div id="inuse_modal" class="quick_modal modal_cont"  style="height:420px;">            	
			<h3 class="modal_title">クイックアクセス　ご利用中のお客さまへ</h3>
			<a class="close_btn close_btn_box ico_close01" href="javascript:void(0);">close</a>
			<div class="quick_modal_inner">
				<div class="fl_content upper mt25">
				
					<p class="basic_title text_bold fl_left">ご利用いただいているお客さまは、便利な会員向けサービスを<br>ご利用いただけます。<br>
				ご利用には会員ページへのログインが必要です。</p>
					<div class="btn_list fl_right">
						<p class="btn type01 navy large single"><a href="../CPA00Control/CPA00001.html" class="ico_lock01 text_color_white">会員ログイン</a></p>
						<p class="btm_text text_center"><span  class="arrow arw01r"><a href="../APD73Control/APD73001.html">会員ページでできること</a></span></p>
					</div>
				</div>

				<ul class="btm btn_list fl_content">
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/APD73Control/APD73006.html">瞬フリ<br>（振込キャッシング）</a></p>
						<p class="btm_text">24時間365日振込！</p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/APD73Control/APD73002.html">限度額<br>変更申込</a></p>
						<p class="btm_text">いざという時の急な出費に</p>
					</li>	
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/APD73Control/APD73005.html">ご返済日<br>お知らせメール</a></p>
						<p class="btm_text">期日を事前にお知らせ</p>
					</li>	
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/APD73Control/APD73003.html">口フリ<br>（口座振替）申込</a></p>
						<p class="btm_text">手数料0円！</p>
					</li>	
				</ul>
			</div>
		</div><!--#inuse_modal  -->
		 
		<div id="first_modal" class="quick_modal modal_cont"  style="height:420px;">            	
			<h3 class="modal_title">クイックアクセス　はじめてのお客さまへ</h3>
			<a class="close_btn close_btn_box ico_close01" href="javascript:void(0);">close</a>
			<div class="quick_modal_inner">

				<h4 class="basic_title">はじめての方向け、かんたん診断はこちら</h4>
				<ul class="upper btn_list fl_content">
					<li>
						<p class="btn type01 lightblue no_grad"><a href="javascript:void(0)" class="check_modal_btn" rel="check_modal01">はじめてのご利用<br>MOVIE</a></p>
						<p class="btm_text"><span class="arrow arw01r"><a href="../Pcmain/APD61Control/APD61001.html">ご利用方法のご案内</a></span></p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="javascript:void(0)" class="check_modal_btn" rel="check_modal02">お借入<br>シミュレーション</a></p>
						<p class="btm_text">借りられるかな？</p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="javascript:void(0)" class="check_modal_btn" rel="check_modal03">ご返済<br>シミュレーション</a></p>
						<p class="btm_text">返せるかな？</p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="javascript:void(0)" class="check_modal_btn" rel="check_modal04">お借入<br>プラン診断</a></p>
						<p class="btm_text">どうやって返すの？</p>
					</li>
				</ul>
				<h4 class="basic_title">お問い合わせ・お申込みはこちら</h4>
				<ul class="btm btn_list fl_content">
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/APD68Control/APD68001.html">よくある<br>ご質問</a></p>
						<p class="btm_text">お問い合わせの多いご質問</p>
					</li>
					<li>
						<p class="btn type01 lightblue no_grad"><a href="/Pcmain/BPD01Control/BPD01001.html">メールで<br>お問い合わせ</a></p>
						<p class="btm_text">よくある質問が<br>見つからなかった場合</p>
					</li>	
					<li>
						<p class="btn type01 large"><a href="/Pcmain/BPA00Control/BPA00001.html" class="ico_pen02 text_color_white"><span class="small">24時間いつでもOK!</span>新規お申込み</a></p>
						<p class="btm_text text_left">24時間いつでもお申込可能です。Web契約の場合には、<br>最短当日中にお振込みでご融資が可能です。</p>
					</li>
				</ul>

			<div id="check_modal_wrap">
				<div class="check_modal fl_content" id="check_modal01">
					<div class="title_area">
					<h3><img src="../image/html/APA00/APA00_main_movie_title.png" alt="部鳥（ブチョー）と課鳥（過チョー）のはじめてのプロミス 体験記！ ">	</h3>
					</div>
					<div class="content fl_content">
						<div class="movie_area" id="movie_tab">
							<div class="movie tab_content cr" id="movie01">
							<iframe width="470" height="260" src="https://www.youtube.com/embed/NqPnyD5GS3s" frameborder="0" allowfullscreen></iframe>
							</div>
							<div class="movie tab_content" id="movie02">
							<iframe width="470" height="260" src="https://www.youtube.com/embed/Lr4_Gi7SxsY" frameborder="0" allowfullscreen></iframe>
							</div>
							<div class="movie tab_content" id="movie03">
							<iframe width="470" height="260" src="https://www.youtube.com/embed/L81gn5Xf864" frameborder="0" allowfullscreen></iframe>
							</div>
							<div class="movie tab_content" id="movie04">
							<iframe width="470" height="260" src="https://www.youtube.com/embed/BPfnc7oPxE0" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<ul class="movie_list tab_menu"  rel="movie_tab">
							<li>
								<a href="javascript:void(0)" rel="movie01" class="cr">
									<img src="../image/html/APA00/APA00_main_movie_thumb01.jpg">
									<p class="double">お申込み・<br>ご契約編&#9312;</p>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" rel="movie02">
									<img src="../image/html/APA00/APA00_main_movie_thumb02.jpg">
									<p class="double">お申込み・<br>ご契約編&#9313;</p>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" rel="movie03">
									<img src="../image/html/APA00/APA00_main_movie_thumb03.jpg">
									<p>お借入れ編</p>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" rel="movie04">
									<img src="../image/html/APA00/APA00_main_movie_thumb04.jpg">
									<p>ご返済編</p>
								</a>
							</li>							
						</ul>						
					</div>
					<a href="javascript:void(0)" class="check_close_btn icon">&#xe920;</a>
				</div><!-- /#check_modal01-->
				
				<div class="check_modal fl_content loan_sim" id="check_modal02">
					<div class="title_area">
						<h3>お借入<br>シミュレーション</h3>
						<p>簡単入力で、プロミスで<br>お借入可能かすぐに結果が<br>わかります。</p>
					</div>
					<div class="content">
						<div class="step step1 sec form">
							<form>
							<div class="input_line fl_content">
								<p class="label">生年月日</p>
								<div class="input">
									<p>
										<input type="text" class="middle ml_none" placeholder="例：1980">年<input type="text" class="small">月<input type="text" class="small">日 </p>
									<p class="notice">20〜69歳の方が対象です。</p>
									<p class="notice">半角数字でご入力ください。</p> 
								</div>
								<p class="error_box" style="display:none;">入力内容に誤りがあります。20〜69歳の方が対象です。半角数字でご入力ください。</p>
							</div>
							<div class="input_line fl_content middle">
								<p class="label">年　　収</p>
								<div class="input">
									<p><input type="text" class="middle ml_none" placeholder="例：400">万円（税込み）</p>
									<p class="notice">半角数字でご入力ください。</p> 
								</div>
								<p class="error_box" style="display:none;">入力内容に誤りがあります。半角数字でご入力ください。</p>

							</div>
							<div class="input_line fl_content middle bd_btm_none">
								<p class="label double">現在の<br>お借入れ金額</p>
								<div class="input">
									<p><input type="text" class="middle ml_none" placeholder="例：10">万円</p>
									<p class="notice">お借入れがない場合は「0」を入力してください。</p>
									<p class="notice">半角数字でご入力ください。</p> 
								</div>
								<p class="error_box"  style="display:none;">入力内容に誤りがあります。半角数字でご入力ください。</p>
							</div>
							<p class="btn type01 green no_grad"><button type="submit" class=""><span class="ico_ck02">シミュレーション実行</span></button></p>
							</form>
						</div>				
						<div class="step step2 sec result_wrap">
							<div class="result">
								<div class="sec_box result_box">
									<h2 class="message"><span>お客さまの条件ですと、</span>ご融資可能と思われます。</h2>
								</div>								
								<div class="fl_content btn_area text_left">
									<p class="guide">このままお申込みを続けると、<br>入力の手間が省けお手続きが<br>簡単になります。</p>
									<p class="btn type01 large form_btn"><a href="./Pcmain/BPA00Control/BPA00001.html" class="ico_pen02 text_color_white"><span class="small">入力内容をそのまま継続して</span>新規お申込み</a></p>
								</div>	
								<p class="notice">診断結果は簡易的なものです。実際のお申込みでは、より詳細なお客さま情報に基づいてご審査いたしますので、ご希望に添えない場合があることをご了承ください。</p>
							</div>				
						</div><!-- /step2 -->

						<div class="step step3 sec result_wrap">
							<div class="result">
								<div class="sec_box result_box improper">
									<h2 class="message">大変申し訳ございませんが、<br>ご入力いただいた内容だけでは判断することができません。</h2>
									<p class="notice">もう少し、詳しくおうかがいすると、ご融資可能か判断することができます。<br>以下の「新規お申込み」ボタンより、お申込みください。</p>
								</div>								
								<div class="fl_content btn_area text_left">
									<p class="guide">このままお申込みを続けると、<br>入力の手間が省けお手続きが<br>簡単になります。</p>
									<p class="btn type01 large form_btn"><a href="./Pcmain/BPA00Control/BPA00001.html" class="ico_pen02 text_color_white"><span class="small">入力内容をそのまま継続して</span>新規お申込み</a></p>
								</div>	
								<p class="notice">診断結果は簡易的なものです。実際のお申込みでは、より詳細なお客さま情報に基づいてご審査いたしますので、ご希望に添えない場合があることをご了承ください。</p>
							</div>				
						</div><!-- /step3 -->

						<div class="step step4 sec result_wrap">
							<div class="result">
								<div class="sec_box result_box limit">
									<h2 class="message">お借入シミュレーションは2回までしか<br>実行することができません。<br>またのご利用をお待ちしております。</h2>
								</div>								
							</div>				
						</div><!-- /step4 -->
												
					</div><!-- /content -->
					<a href="javascript:void(0)" class="check_close_btn icon">&#xe920;</a>
				</div><!-- /#check_modal02-->


				<!================= loan_plan start =================>				
				<div class="check_modal fl_content repayment_sim" id="check_modal03">
					<div class="title_area">
						<h3>ご返済<br>シミュレーション</h3>
						<p>ご希望に応じたご返済期間・<br>ご返済金額・お借入希望額を<br>入力すると返済計画を<br>シミュレーション出来ます。</p>
					</div>
					<div class="content">
						<nav class="tab_menu" rel="repayment_tab">
							<ul class="fl_content">
								<li><a href="javascript:void(0)" class="cr" rel="repayment_tab01"><span>ご返済金額シミュレーション</span></a></li>
								<li><a href="javascript:void(0)" rel="repayment_tab02"><span>ご返済期間シミュレーション</span></a></li>
								<li><a href="javascript:void(0)" rel="repayment_tab03"><span>お借入可能額シミュレーション</span></a></li>
							</ul>
						</nav>
						
						<div id="repayment_tab" class="tab_contents">
	
							<!-- 返済金額シミュレーション -->
							<div id="repayment_tab01" class="tab_content cr">
								<div class="step step1 active sec form">
									<form>
									<p class="mb10">お借入希望額をご指定の期間（月数）で返済するときの、毎月のご返済金額を算出します。</p>
									<table class="simulation_box">
										<tr>
											<th>お借入希望額</th>
											<th>ご返済期間</th>
											<th>お借入金利（年率）</th>
										</tr>
										<tr>
											<td>							
												<p>
													<input type="text" class="small error" placeholder="">万円
												</p>
												<p class="error_box">半角数字でご入力ください。</p>
												<p class="notice small">半角数字で入力してください。</p>
											</td>
											<td>							
												<p>
													<input type="text" class="small" placeholder="例：12">ヶ月
												</p>
												<ul class="notice small">
													<li>半角数字で入力してください</li>
													<li>80ヶ月以内で入力できます。</li>
												</ul>
											</td>
											<td>							
												<p>
													<input type="text" class="small" placeholder="17.8">%
												</p>
												<ul class="notice small">
													<li>半角数字で入力してください。</li>
													<li>フリーキャッシングは借入利率17.8％です。</li>
												</ul>
											</td>
										</tr>
									</table>
									<p class="notice small">計画的なご返済を行っていただくために、お借入希望額が30万円以下の場合のご返済期間は、36ヶ月以内で入力してください。</p>
									<div class="text_center mt10">
										<p class="btn type01 green no_grad form_btn"><button type="submit" name="" id=""  class="large ico_ck02">シミュレーション実行</button></p>
									</div>						
									</form>
								</div><!-- /.step1 -->
								<div class="step step2 sec form">
									<p class="mb10">お借入希望額をご指定の期間（月数）で返済するときの、毎月のご返済金額を算出します。</p>
									<table class="result simulation_box">
										<tr>
											<th>お借入希望額</th>
											<th>ご返済期間</th>
											<th>お借入金利（年率）</th>
										</tr>
										<tr>
											<td>							
												<p>
													<span class="num">10</span>万円
												</p>
											</td>
											<td>							
												<p>
													<span class="num">12</span>ヶ月
												</p>
											</td>
											<td>							
												<p>
													<span class="num">17.8</span>%
												</p>
											</td>
										</tr>
									</table>
									<p class="notice small mb5">フリーキャッシングは借入利率17.8％です。</p>
									<div class="sec_box result_box mb5">
										毎月のご返済金額は<span class="num">9,158円</span>
									</div>
									<p class="lineheight12">
										<span class="small notice mr10">最終回のご返済金額は端数調整のため多少変動します。</span>
										<span class="small notice mr10">実際のご利用の際は、月の日数の相違などにより、この表の金額とは多少異なる場合があります。</span>
										<span class="small notice mr10">うるう年は366日の日割り計算となります。</span>
									</p>
									<div class="text_center mt10">
										<p class="btn type01 green no_grad form_btn"><a href="#repayment_modal" class="open_modal large ico_ck02">返済計画の詳細を見る</a></p>
									</div>			
								</div><!-- /.step2 -->
							</div><!--/#repayment_tab01 -->
							
	
	
	
							<!-- ご返済期間シミュレーション -->
							<div id="repayment_tab02" class="tab_content">
	
								<div class="step step1 active sec form">
									<form>
									<p class="mb10">お借入希望額を毎月一定の金額で返済するときの、ご返済期間（月数）を算出します。</p>
									<table class="simulation_box">
										<tr>
											<th>お借入希望額</th>
											<th>毎月のご返済金額</th>
											<th>お借入金利（年率）</th>
										</tr>
										<tr>
											<td>							
												<p>
													<input type="text" class="small error" placeholder="">万円
												</p>
												<p class="error_box">半角数字でご入力ください。</p>
												<p class="notice small">半角数字で入力してください。</p>
											</td>
											<td>							
												<p>
													<input type="text" class="small" placeholder="例：8000">円
												</p>
												<p class="notice small">半角数字で入力してください。</p>
											</td>
											<td>							
												<p>
													<input type="text" class="small" placeholder="17.8">%
												</p>
												<ul class="notice small">
													<li>半角数字で入力してください。</li>
													<li>フリーキャッシングは借入利率17.8％です。</li>
												</ul>
											</td>
										</tr>
									</table>
									<div class="text_center mt35">
										<p class="btn type01 green no_grad form_btn"><button type="submit" name="" id=""  class="large ico_ck02">シミュレーション実行</button></p>
									</div>						
									</form>
								</div><!-- step1 -->
								
								<div class="step step2 sec form">
									<p class="mb10">お借入希望額をご指定の期間（月数）で返済するときの、毎月のご返済金額を算出します。</p>
									<table class="result simulation_box">
										<tr>
											<th>お借入希望額</th>
											<th>毎月のご返済金額</th>
											<th>お借入金利（年率）</th>
										</tr>
										<tr>
											<td>							
												<p>
													<span class="num">10</span>万円
												</p>
											</td>
											<td>							
												<p>
													<span class="num">8000</span>円
												</p>
											</td>
											<td>							
												<p>
													<span class="num">17.8</span>%
												</p>
											</td>
										</tr>
									</table>
									<p class="notice small mb5">フリーキャッシングは借入利率17.8％です。</p>
									<div class="sec_box result_box mb5">
										ご返済終了まで　<span class="num">14ヶ月</span>
									</div>
									<p class="lineheight12">
										<span class="small notice mr10">最終回のご返済金額は端数調整のため多少変動します。</span>
										<span class="small notice mr10">実際のご利用の際は、月の日数の相違などにより、この表の金額とは多少異なる場合があります。</span>
										<span class="small notice mr10">うるう年は366日の日割り計算となります。</span>
									</p>
									<div class="text_center mt10">
										<p class="btn type01 green no_grad form_btn"><a href="#repayment_modal" class="open_modal large ico_ck02">返済計画の詳細を見る</a></p>
									</div>			
								</div><!-- step2 -->
							</div><!--/#repayment_tab02 -->	
	
							<!-- お借入可能額シミュレーション -->
							<div id="repayment_tab03" class="tab_content">
	
								<div class="step step1 active sec form">
									<form>
									<p class="mb10">毎月のご返済可能額とご返済期間（月数）を設定したときの、お借入可能額を算出します。</p>
									<table class="simulation_box">
										<tr>
											<th>毎月のご返済金額</th>
											<th>ご返済期間</th>
											<th>お借入金利（年率）</th>
										</tr>
										<tr>
											<td>							
												<p>
													<input type="text" class="small" placeholder="例：8000">円
												</p>
												<p class="notice small">半角数字で入力してください。</p>
											</td>
											<td>							
												<p>
													<input type="text" class="small" placeholder="例：12">ヶ月
												</p>
												<ul class="notice small">
													<li>半角数字で入力してください</li>
													<li>80ヶ月以内で入力できます。</li>
												</ul>
											</td>
											<td>							
												<p>
													<input type="text" class="small" placeholder="17.8">%
												</p>
												<ul class="notice small">
													<li>半角数字で入力してください。</li>
													<li>フリーキャッシングは借入利率17.8％です。</li>
												</ul>
											</td>
										</tr>
									</table>
									<p class="notice small">計画的なご返済を行っていただくために、お借入希望額が30万円以下の場合のご返済期間は、36ヶ月以内で入力してください。</p>
									<div class="text_center mt10">
										<p class="btn type01 green no_grad form_btn"><button type="submit" name="" id=""  class="large ico_ck02">シミュレーション実行</button></p>
									</div>						
									</form>
								</div>
								<div class="step step2 sec form">
									<p class="mb10">お借入希望額をご指定の期間（月数）で返済するときの、毎月のご返済金額を算出します。</p>
									<table class="result simulation_box">
										<tr>
											<th>毎月のご返済金額</th>
											<th>ご返済期間</th>
											<th>お借入金利（年率）</th>
										</tr>
										<tr>
											<td>							
												<p>
													<span class="num">8000</span>円
												</p>
											</td>
											<td>							
												<p>
													<span class="num">12</span>ヶ月
												</p>
											</td>
											<td>							
												<p>
													<span class="num">17.8</span>%
												</p>
											</td>
										</tr>
									</table>
									<p class="notice small mb5">フリーキャッシングは借入利率17.8％です。</p>
									<div class="sec_box result_box mb5">
										お借入可能額は<span class="num">87,400円</span>
									</div>
									<p class="lineheight12">
										<span class="small notice mr10">最終回のご返済金額は端数調整のため多少変動します。</span>
										<span class="small notice mr10">実際のご利用の際は、月の日数の相違などにより、この表の金額とは多少異なる場合があります。</span>
										<span class="small notice mr10">うるう年は366日の日割り計算となります。</span>
									</p>
									<div class="text_center mt10">
										<p class="btn type01 green no_grad form_btn"><a href="#repayment_modal" class="open_modal large ico_ck02">返済計画の詳細を見る</a></p>
									</div>			
								</div><!-- step3 -->
							</div><!--/#repayment_tab03 -->	
												
						</div><!-- ./tab_contents -->
								
					</div><!-- ./content -->
					
					<a href="javascript:void(0)" class="check_close_btn icon">&#xe920;</a>
				</div><!-- /#check_modal03-->
				<!================= loan_plan start =================>				



				<div class="check_modal fl_content loan_plan" id="check_modal04">
					<div class="title_area">
						<h3>お借入<br>プラン診断</h3>
						<p>あなたにぴったりのお借入<br>プランを診断できます</p>
					</div>
					<div class="content">
						<div class="step step1 active">
							<p class="question">お客様に当てはまる方をお選びください</p>
							<div class="btn_wrap">
								<a href="javascript:void(0);" data-step="1" data-goto="2" data-select="1" class="select"><span class="ico_ck02">今すぐ<br>借りたい</span></a>
								<a href="javascript:void(0);" data-step="1" data-goto="2" data-select="2" class="select"><span class="ico_ck02">特に急ぎ<br>ではない</span></a>
							</div>
						</div><!-- /.step1-->		
						<div class="step step2">
							<div class="btn_wrap">
								<p class="question">お客様に当てはまる方をお選びください</p>
								<a href="javascript:void(0);" data-step="2" data-goto="3" data-select="1" class="select"><span class="ico_ck02">家から<br>返したい</span></a>
								<a href="javascript:void(0);" data-step="2" data-goto="3" data-select="2" class="select"><span class="ico_ck02">直接現金で<br>返したい</span></a>
							</div>
							<div class="return_btn">
								<p class="btn type01 gray no_grad thin w_auto"><a href="javascript:void(0);" class="auto return" data-goto="1"  data-reset="0">1つ戻る</a></p>
							</div>
						</div><!-- /.step2-->	
						<div class="step step3 result">
							<h5 class="intro">お客さまにぴったりのお借入プランは…</h5>
							<div class="result_box fl_content">
								<figure class="img"><img src="../image/html/APA00/APA00001_loan_plan_result01.png" alt="" /></figure>
								<div class="result_content">
									<div  class="ml25">
										<h4 class="result_title">Webからのお申込み</h4>
										<p class="mb10">Webから、24時間365日、いつでも好きなときにご自宅でお申込みいただけます。</p>
										<p class="text_right mb15"><span class="arrow arw01r"><a href="#">詳細をご覧になりたい方</a></span></p>
									</div>
									<section class="form application">
										<div class="btn_list">
											<p class="btn type01 large"><a href="http://promise.tribeck.com/Pcmain/BPA00Control/BPA00001.html" class="ico_pen02 text_color_white" target="_blank"><span class="small">24時間いつでもOK!</span>新規お申込み</a></p>
											<p class="btn type01 gray no_grad fl_right"><a href="javascript:void(0);" class="return"  data-goto="1"  data-reset="1">もう一度<br>最初からやり直す</a></p>
										</div>
									</section>
								</div>
							</div>	
						</div><!-- /.step3-->
					</div><!-- /.content-->
					<a href="javascript:void(0)" class="check_close_btn icon return"  data-goto="1"  data-reset="1">&#xe920;</a>
				</div><!-- /#check_modal04-->
				<!================= loan_plan end =================>				
				

			</div><!-- /#check_modal_wrap-->
			
			</div><!-- /#quick_modal_inner-->


		</div><!-- /.modal_cont-->




		<div id="repayment_modal" class="modal_cont">            	
			<h3 class="modal_title">返済計画の詳細 </h3>
			<a class="inner_close_btn close_btn_box ico_close01" href="javascript:void(0);">close</a>
			<div class="scroll">
			<div class="sec mb_none">
				<h4 class="bg_title">返済計画表</h4>
				<table class="chart text_center">
					<tr>
						<th class="w50">回数</th>
						<th class="w_auto">返済額</th>
						<th class="w_auto">元金</th>
						<th class="w_auto">利息</th>
						<th class="w_auto">残高</th>
					</tr>
					<tr>
						<td class="bg_lightblue04">1</td>
						<td>9,158</td>
						<td>7,675</td>
						<td>1,483</td>
						<td>92,325</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">2</td>
						<td>9,158</td>
						<td>7,789</td>
						<td>1,369</td>
						<td>84,536</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">3</td>
						<td>9,158</td>
						<td>7,905</td>
						<td>1,253</td>
						<td>76,631</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">4</td>
						<td>9,158</td>
						<td>8,022</td>
						<td>1,136</td>
						<td>68,609</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">5</td>
						<td>9,158</td>
						<td>8,141</td>
						<td>1,017</td>
						<td>60,468</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">6</td>
						<td>9,158</td>
						<td>8,262</td>
						<td>896</td>
						<td>52,206</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">7</td>
						<td>9,158</td>
						<td>8,384</td>
						<td>774</td>
						<td>43,822</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">8</td>
						<td>9,158</td>
						<td>8,508</td>
						<td>650</td>
						<td>35,314</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">9</td>
						<td>9,158</td>
						<td>8,635</td>
						<td>523</td>
						<td>26,679</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">10</td>
						<td>9,158</td>
						<td>8,763</td>
						<td>395</td>
						<td>17,916</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">11</td>
						<td>9,158</td>
						<td>8,893</td>
						<td>265</td>
						<td>9,023</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">12</td>
						<td>9,156</td>
						<td>9,023</td>
						<td>133</td>
						<td>0</td>
					</tr>
					<tr>
						<td class="bg_lightblue04">累計</td>
						<td>109,894</td>
						<td>100,000</td>
						<td>9,894</td>
						<td>0</td>
					</tr>
				</table>
				<div class="text_center btn_area">
					<p class="btn type01 large mb15 inline_block"><a href="/Pcmain/BPA00Control/BPA00001.html" class="ico_pen02 text_color_white"><span class="small">24時間いつでもOK！</span>新規お申込み</a></p>

					<p>24時間いつでもお申込可能です。Web契約の場合には、最短当日中にお振込みでご融資が可能です。</p>
				</div>
			</div>
			</div>
		</div>



      </div>
	  <div class="bg close_btn"></div>
</div>





