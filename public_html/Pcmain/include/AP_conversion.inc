<div id="footer_simulation" class="fl_content">
		<div class="inner">
			<p class="call">
					<img src="/Pcmain/common/co_footer_call.png" alt="ご相談・ご質問は プロミスコールへ 0120-24-0365" >
					<img src="/Pcmain/common/co_footer_call_ladies.png" alt="女性専用ダイヤル レディースコール 0120-86-2634" >	
			</p>
			<ul class="fl_content btn_list">
				<li>
					<p class="btn type01 lightblue no_grad"><a href="http://promise.tribeck.com/Pcmain/BPB00Control/BPB00001.html">お借入<br>シミュレーション</a></p>
					<p class="btm_text">借りられるかな？</p>
				</li>
				<li>
					<p class="btn type01 lightblue no_grad"><a href="http://promise.tribeck.com/Pcmain/BPB01Control/BPB01001.html">ご返済<br>シミュレーション</a></p>
					<p class="btm_text">返せるかな？</p>
				</li>
				<li>
					<p class="btn type01 lightblue no_grad"><a href="http://promise.tribeck.com/Pcmain/APD61Control/APD61015.html">お借入<br>プラン診断</a></p>
					<p class="btm_text">どうやって返すの？</p>
				</li>
				<li>
					<p class="btn type01 large"><a href="http://promise.tribeck.com/Pcmain/BPA00Control/BPA00001.html" class="ico_pen02 text_color_white"><span class="small">24時間いつでもOK!</span>新規お申込み</a></p>
					<p class="btm_text text_left"><span class="arrow arw01r"><a href="/Pcmain/APD61Control/APD61006.html" class="orange">店舗・ATMでお申込み</a></span><span class="arrow arw01r"><a href="#" class="orange">お電話でお申込み</a></span></p>
				</li>
			</ul>
		</div>
	</div>