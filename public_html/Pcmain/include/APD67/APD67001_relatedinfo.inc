<div id="footer_contentslink" class="fl_content">
		<div class="inner">
			<div class="fl_left box">
				<h5 class="ico_qt02">よくあるご質問</h5>
				<ul>
					<li class="arrow arw01r"><a href="#">どこで返済できるのですか？ </a></li>
					<li class="arrow arw01r"><a href="#">インターネットからしたいのですが？ </a></li>
					<li class="arrow arw01r"><a href="#">もし期日に遅れそうな場合は？ </a></li>
					<li class="arrow arw01r"><a href="#">期日に遅れてしまった 場合はどうなりますか？ </a></li>
					<li class="arrow arw01r"><a href="#">返済額や利息を知りたいのですが？ </a></li>
				</ul>
				<p class="gotoindex"><span class="arrow arw01r"><a href="#">よくあるご質問一覧へ</a></span></p>
			</div>
			<div class="fl_left box">
				<h5 class="ico_star01">おすすめコンテンツ</h5>
				<ul>
					<li class="arrow arw01r"><a href="../APD64Control/APD64002.html">ご返済について</a></li>
					<li class="arrow arw01r"><a href="../APD64Control/APD64003.html">ご返済期日について</a></li>
					<li class="arrow arw01r"><a href="../APD64Control/APD64004.html">ご返済金額について</a></li>
					<li class="arrow arw01r"><a href="../BPB01Control/BPB01001.html">ご返済シミュレーション</a></li>
				</ul>
			</div>
		</div>
	</div>