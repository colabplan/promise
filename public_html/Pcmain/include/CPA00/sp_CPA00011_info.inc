		<p class="goindex"><a href="#" class="arrow arw02r">一覧を見る</a></p>
		<ul class="news">
					<li>プロミスからのお知らせがございます。<a href="../CPL00Control/CPL00001_sp.html">プロミスからのお知らせ</a></li>
					<li>ご登録いただいているメールアドレスが無効です。<a href="../CPC00Control/CPC00001_sp.html">メールアドレス登録・変更</a></li>
					<li>当社が法律にもとづき交付する書面は郵送します。<br>書面の受取方法の変更をご希望の際は、こちらよりご変更いただけます。<a href="../CPD02Control/CPD02001_sp.html">ご利用明細書受取方法登録・変更 </a></li>
					<li>ご確認されていないお取引明細書がございます。<a href="../CPM01Control/CPM01001_sp.html">お取引明細書ダウンロード</a></li>
					<li>ご確認されていない契約内容確認書がございます。<a href="#">契約内容確認書ダウンロード</a></li>
		</ul>