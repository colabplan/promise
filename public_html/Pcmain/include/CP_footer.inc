	<div id="footer_attention">
		<div class="inner">
			<h3>契約内容をよくご確認ください。収支と支出のバランスを大切に。無理のない返済計画を。</h3>
			<div class="fl_content">
				<p class="text">キャッシング、カードローン、消費者金融のお申込みはプロミスにおまかせください！ＳＭＦＧグループの金融会社ＳＭＢＣコンシューマーファイナンスのプロミスで安心のカードローン♪ご返済やキャッシング・ローンに便利なご利用方法を豊富にご用意しています。プロミスは24時間お申込みOK、スピーディーな審査でお答えします。パソコン、ケータイ、スマートフォン、お客様サービスプラザ（店頭窓口）、ATM、コンビニなどで24時間、キャッシング・ローンのご返済が可能です。毎月ご返済日にお客さまがお持ちの口座から自動的にご返済額が引落しできる口フリや、24時間365日、お客さまの口座へ振込キャッシングする瞬フリなど、プロミスインターネット会員サービスも、ご利用いただけます。</p>
				<p class="marks">
					<a href="http://privacymark.jp/" target="_blank"><img src="/Pcmain/common/co_privacymark.png" alt="プライバシーマーク 10660070(05)"  class="mark"></a>
					<a href="https://trustsealinfo.websecurity.norton.com/splash?form_file=fdf/splash.fdf&dn=cyber.promise.co.jp&lang=ja" target="_blank"><img src="/Pcmain/common/co_norton_secure.png" alt="Norton SECURED powered by Symantec"  class="mark"></a>
				</p>
			</div>
		</div>
	</div>
	<div id="footer_bottom">
		<div class="inner">
			<nav class="utility">
				 <span class="external" ><a href="/Pcmain/APD04Control/APD04001.html"target="_blank">個人情報保護方針</a></span><span class="external" ><a href="/Pcmain/APA00Control/APA00015.html" target="_blank">反社会的勢力に対する基本方針</a></span><span class="external" ><a href="/Pcmain/APD03Control/APD03001.html" target="_blank">サイトのご利用について</a></span>
			</nav>
			<p id="copyright">ＳＭＢＣコンシューマーファイナンス株式会社 電話：03-3543-7100（代表） 登録番号：関東財務局長（11）第00615号 日本貸金業協会会員 第000001号 <br>
Copyright SMBC Consumer Finance Co., Ltd. All rights reserved.</p>
		</div>	
	</div>