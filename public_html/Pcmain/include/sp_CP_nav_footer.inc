<!================= footer start =================>				
<footer id="footer">	
	<div id="footer_upper" class="inner">			
		<nav class="utility">
			 <ul>
			 	<li><a href="/Pcmain/APD04Control/APD04001_sp.html" class="external" target="_blank">個人情報保護方針</a></li>
			 	<li><a href="/Pcmain/APA00Control/APA00015_sp.html" class="external" target="_blank">反社会的勢力に対する基本方針</a></li>
			 	<li><a href="/Pcmain/APD03Control/APD03001_sp.html" class="external" target="_blank">サイトのご利用について</a></li>
			 </ul>
		</nav>
		<p id="to_pc"><a href="/Pcmain/CPA00Control/CPA00011.html"><span  class="ico_pc01">PCサイトTOP</span></a></p>
		<div class="marks gray_box">
			<img src="/Pcmain/common/sp/co_sp_privacymark.png" alt="プライバシーマーク 10660070(05)"  class="mark">
			<img src="/Pcmain/common/sp/co_sp_norton_secure.png" alt="Norton SECURED powered by Symantec"  class="mark">
			<p>プロミスを展開するＳＭＢＣコンシューマーファイナンスはＳＭＦＧグループの一員です。</p>
		</div>
		<h3 class="attention gray_box">契約内容をよくご確認ください。<br>収支と支出のバランスを大切に。<br>無理のない返済計画を。</h3>
	</div>
	<div id="footer_btm">
		<p class="inner registration">ＳＭＢＣコンシューマーファイナンス株式会社 <br>電話：03-3543-7100（代表） <br>登録番号：関東財務局長（11）第00615号 <br>日本貸金業協会会員 第000001号</p>
	</div>
<p id="copyright"> Copyright SMBC Consumer Finance Co., Ltd. <br>All rights reserved.</p>
</footer>				
<!================= footer end =================>	