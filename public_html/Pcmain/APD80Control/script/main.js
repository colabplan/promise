!function(window, document, $){
    "use strict";
	 function init() {
			$(window).load(function(){
				$(".contents").addClass('show');
				$('[class^=match_h]').matchHeight();

				//カルーセル
				$('.carousel .list').slick({
				  slidesToShow: 3,
				  slidesToScroll: 1,
				  autoplay: true,
				  autoplaySpeed: 5000,
				  speed : 200,
				  useCSS : false,
				  dots: true,
				  draggable : false,
				  prevArrow : '<a href="javascript:void(0);" class="navigation prev icon"><span>&#xe918;</span></a>',
				  nextArrow : '<a href="javascript:void(0);" class="navigation next icon"><span>&#xe91a;</span></a>',
				  
				});
				$(document).on("change",".fileinput",function() {
					var ID = $(this).attr("id");
					var fileName = document.getElementById(ID).value;
					fileName=fileName.replace(/C:\\fakepath\\/g, "" ); 
					$('.filewrap[rel="'+ID+'"]' ).find(".filename").html(fileName);
				});
				$(".filedelete").click(function() {
					var ID = $(this).attr("rel");
					$('.filewrap[rel="'+ID+'"]' ).find(".filename").html("&nbsp;");
					$("#"+ID).replaceWith('<input type="file" name="'+ID+'" class="fileinput" id="'+ID+'">');
				});

			});
			gNav.init();
			sNav.init();
			fixHeader.init();
			smScroll.init('scroll','#header',69,500);
			bnrCarousel.init();
			accd.init();
			tab.init();
			process.init();
			checkModal.init();
			loanPlan.init();
			repaymentSim.init();
			loanSim.init();
			toggleModal.init();
			quickAccessFix.init(950,30);
			newWindow.init(800,600);

	 }	
	 var gNav = {},
			 sNav = {},
			fixHeader = {},
			smScroll = {},
			bnrCarousel = {},
			accd = {},
			tab = {},
			process = {},
			checkModal = {},
			loanPlan = {},
			repaymentSim = {},
			loanSim = {},
			toggleModal = {},
			quickAccessFix= {},
			newWindow = {};


	/* ===================
		グロナビ
		=================== */
	gNav = {
		init:function (){
			var $gNavLink,$navLink,$navListIsSub,gNavRel;
			$gNavLink = $("#global_navigation li a.global");
			$navListIsSub = $("#global_navigation li.is_sub");
			$navLink = $("#global_navigation li.is_sub a.global");
			gNavRel = $("body").attr("rel");
		  //crクラス付加
			if(gNavRel) {
				  $gNavLink.each(function(){		  		
			    	var urlMain = $(this).attr("rel");
			    	var match = gNavRel.match(urlMain);
			    	var matchResult = match == null ? 0 : 1
					if(matchResult) {
						$(this).addClass('cr');
					}
				});
		  }
		  //タブレット判別
		  var md = new MobileDetect(window.navigator.userAgent);
		  	if(md.tablet()!= null){
			    $navLink.on("click",
			    	function () {
							$navListIsSub.removeClass("active");
							$(this).parent().addClass("active");
							return false;
						} 
					);					
				$("*:not(.sublink,.global)").on("click",
			    	function () {
							$navListIsSub.removeClass("active");
						} 
					);		
		  	}else {
				//マウスオーバー / アウト時
			    $navListIsSub .hover(
			    	function () {
							$(this).addClass("active");
						}, 
					function () {
							$navListIsSub.removeClass("active");
						}
					);	
				}
			}
	},
	/* ===================
		サブナビ
		=================== */
	sNav = {
		init:function (){
		var $localNavList;
		var localNavBool = false;
		$localNavList = $("#local_navigation li");

		$(window).load(function () {
		if($localNavList[0]) localNavBool = true;
			if(localNavBool) {
				  //crクラス付加
				  var crPath = location.pathname;
				  $localNavList.each(function(){
				    	var urlMain = "__null_main";
				    	var urlParent= "__null_parent";
				    	if($(this).data('link')) urlMain = $(this).data('link');				    	
				    	var match = crPath.match(urlMain);
				    	var matchResult = match == null ? 0 : match.index;
						if(matchResult) {
							$(this).addClass('cr');
							if($(this).data('parent')) urlParent = $(this).data('parent');
							$('[data-link="'+urlParent+'"]').addClass('cr');
						}
				  });
				}
			});
		}	
	},
		/* ===================
		メニュー固定
		=================== */	
	fixHeader = {
		init:function() {
			if(!$("body").hasClass("nofix")) {
			    $(window).on('load scroll', fixHeader.fixed);	
			 }
		 },
		 off:function() {
			$(window).off('load scroll' ,  fixHeader.fixed);			 
		 },
		 fixed:function() {
		    	var h = $('#header').height();
		    	var s = $(window).scrollTop() ;
		        if (s > h +63) {
		            $('#header').addClass('fixed');
		        } else {
		            $('#header').removeClass('fixed');
		        }
		 }		 
	},		
		/* ===================
		スムーススクロール smScroll
		=================== */
	smScroll= {
		init:function(cls ,targetEle,  fix ,duration) {
		 $('a.'+ cls + '[href^=#]').click(function(){
		    var target;
		    target = $( $(this).attr('href') );
		    if (target.length == 0) {
		      target = $(targetEle);
		    }
		    $('html, body').animate({scrollTop: target.offset().top - fix},duration);
		    	return false;
			});
		 }
	},
	
	/* ===================
	バナーカルーセル bnrCarousel
	=================== */
	bnrCarousel= {
		init:function() {

			/*var $carousel,$carouselWrap,$carouselList,$carouselEle,$carouselCnt,$carouselNav,wrapWidth,wrapHeight,coulmn,cntItem,cntNum,cntHtml;
			$carousel = $(".carousel");
			$carouselWrap = $(".carousel .wrap");
			$carouselList = $(".carousel .list");
			$carouselEle = $(".carousel .item");
			$carouselNav = $(".carousel .navigation");
			$carouselCnt = $(".carousel .control");
			cntHtml = "";
			cntItem = '<a href="javascript:void(0);">●</a>';
			wrapWidth = 0;
			wrapHeight= 0;
			cntNum = $carouselEle.length;

			setTimeout(function(){
				$carouselEle.each(function(){
					wrapWidth = $(this).outerWidth(true);
					wrapHeight = $(this).outerHeight(true);
					cntHtml += cntItem;		
				});
				$carouselEle.eq(0).addClass("cr");
				$carouselCnt[0].innerHTML = cntHtml;	
				$carousel.addClass('active');
				$carouselList.width(wrapWidth*cntNum);
				$carouselList.height(wrapHeight);
				$carouselWrap.height(wrapHeight);	
				$carouselCnt.find("a:first").addClass("cr");					
				$carouselNav.click(
					function(){
							var crIndex = $carouselCnt.find("a").index( $carouselCnt.find(".cr"));
							var gotoIndex;
							var direction;
							if($(this).hasClass("prev")) {
								gotoIndex = crIndex  > 0 ? crIndex - 1  : cntNum -1;
								direction = -1;
							}
							if($(this).hasClass("next")) {
								gotoIndex = crIndex  < cntNum - 1 ? crIndex +1  :  0;
								direction = 1;
							}
							bnrCarousel.goTo(gotoIndex,direction,wrapWidth,$carouselEle,$carouselList,$carouselCnt);
					}				
				);
				$carouselCnt.find("a").click(
					function(){
						var crIndex = $carouselCnt.find("a").index( $carouselCnt.find(".cr"));
						var gotoIndex = $carouselCnt.find("a").index( $(this));
						var direction = gotoIndex > crIndex ? 1 : -1;
						bnrCarousel.goTo(gotoIndex,direction,wrapWidth,$carouselEle,$carouselList,$carouselCnt);
					}				
				);
			},100);			
		 },
		 goTo:function(gotoIndex,direction,wrapWidth,$carouselEle,$carouselList,$carouselCnt) {		 	
				$carouselEle.eq(gotoIndex).css("left", direction*wrapWidth + "px");		
				$carouselList.animate({"left" : -1*direction*wrapWidth  +"px"},function(){
					$carouselCnt.find("a.cr").removeClass("cr");
					$carouselCnt.find("a:nth-child("+(gotoIndex + 1)+")").addClass("cr");
					$carouselEle.removeClass("cr").css("left", "-9999px");
					$carouselEle.eq(gotoIndex).css("left",0).addClass("cr");
					$carouselList.css("left",0);
				});		 */
		 }
	},
		/* ===================
		アコーディオン accd
		=================== */
	accd= {
		init:function() {
			$(".accd_btn").each(function(){
				if(!$(this).hasClass("open")) {
					var rel = $(this).attr("rel");
					$('.accd_contents[rel$="'+rel+'"]').hide();
				}
			});
			$(".accd_btn").click(
				function(){
					var $btn =  $(this);
					var rel = $(this).attr("rel");
					if($btn.hasClass("hide")) {
						$('.accd_contents[rel$="'+rel+'"]').fadeIn(500);
						$btn.fadeOut(500);

					} else {
						$('.accd_contents[rel$="'+rel+'"]').slideToggle(500,function(){
							if($btn.hasClass("open")) {
								var txt =$btn.attr("data-closed");
								$btn.find("span.change").text(txt);
							}else{
								var txt =$btn.attr("data-opened");
								$btn.find("span.change").text(txt);		
							}
							$btn.toggleClass("open");
						});
					}
				}
			);
		 }
	},
		/* ===================
		タブ tab
		=================== */
	tab = {
		init:function() {
			$(".tab_content").not(".cr").hide();	
			$(".tab_menu a").click(
				function(){
					$(this).parents(".tab_menu").find("a").removeClass("cr");
					var relTab = $(this).parents(".tab_menu").attr("rel");
					var relContent  = $(this).attr("rel");
					$("#"+relTab + " .tab_content").hide().removeClass("cr");
					$(this).addClass("cr");
					$("#"+relContent).fadeIn().addClass("cr");
				}
			);
		 }
	},	
		/* ===================
		process
		=================== */
	process = {
		init:function() {
			$(".process_btn").click(
				function(){
					var contentH =  $(this).next(".tab_content").height();
					$(this).parents(".process_list").css("padding-bottom",(contentH+36) + "px");
					if($(this).hasClass("next")) {
						var target = $(this).data("target");
						var targetList = $(this).data("next");
						$(this).parents(".flow_item").addClass("active");
						var $flowItem = $(this).parents(".flow_item").nextAll(".flow_item");
						$flowItem.removeClass("active");
						$flowItem.find(".active").removeClass("active");
						$flowItem.find(".tab_content").removeClass("cr").hide();
						$flowItem.find(".process_btn").removeClass("cr");					
						$flowItem.find(".process_list").css("padding-bottom","0px");
						$(target + " .title_wrap").addClass("active");
						$(target + " .process_wrap").each(function(){
							if($(this).is(targetList)) {
								$(this).addClass("active");
							}else {
								$(this).removeClass("active");
							}
						})
					}
				}
			);
		 }
	},	
		/* ===================
		トップページ メインビジュアル モーダル　 checkModal
		=================== */
	checkModal = {
		init:function() {
			$(".check_modal_btn").click(
				function(){
					var $thisEle =$(this);
					$("#check_modal_wrap .check_modal").hide().removeClass("active");
					$(".check_modal_btn").removeClass("active");
					$thisEle.addClass("active");
					var relModal = $thisEle.attr("rel");					
					$("#"+relModal).fadeIn().addClass("active");
					$("#check_modal_wrap").fadeIn();
					loanPlan.reset();
					repaymentSim.reset();
					repaymentSim.resetTab();
					loanSim.reset();
				}
			);
			$(".check_modal .check_close_btn , #main_tab .tab_menu a").click(function(){
					$("#check_modal_wrap").fadeOut();
					$("#check_modal_wrap .check_modal").hide().removeClass("active");
					$(".check_modal_btn").removeClass("active");
					loanPlan.reset();
					repaymentSim.reset();
					repaymentSim.resetTab();
					loanSim.reset();
			});
		 }
	},	
		/* ===================
		お借入プラン診断　 loanPlan
		=================== */
	loanPlan = {
        init: function () {
			var loanPlanSelected = new Array();
			
			$(".loan_plan a.return").click(function(){
				var gotostep = $(this).data("goto");
				var reset = $(this).data("reset");
				if( reset == 1) {
					loanPlan.reset();
				}else {
					$(this).siblings().removeClass("active");
					$(this).addClass("active");
					$(this).parents(".step").removeClass("active");
				}
				$(".step" + gotostep).addClass("active");
			});
			$(".loan_plan a.select").click(function(){
				var step = $(this).data("step");
				var select = $(this).data("select");
				var gotostep = $(this).data("goto");
				loanPlanSelected[step -1] = select;
				$(this).siblings().removeClass("active");
				$(this).addClass("active");
				$(this).parents(".step").removeClass("active");
				$(this).parents(".content").find(".step" + gotostep).addClass("active");
			});
		},
		reset:function() {
			$(".loan_plan .active").removeClass("active");
			$(".step1").addClass("active");
			loanPlan.loanPlanSelected= [];				
		}
	},
		/* ===================
		ご返済シミュレーション　 repaymentSim
		=================== */
	repaymentSim = {
        init: function () {
        
        	$(".inner_close_btn").click(function(){
	        	$("#repayment_modal").hide();
	        	$("#first_modal").show();
        	});
			$(".repayment_sim form").submit(function(){
				var $step = $(this).parent(".step");
				$step.removeClass("active");
				$step.next(".step").addClass("active");
				return false;
			});
			$(".repayment_sim .tab_menu a").click(function(){
					repaymentSim.reset();
			});
		},
		reset:function() {			
			$(".repayment_sim .step").removeClass("active");
			$(".repayment_sim .step1").addClass("active");
		},
		resetTab:function() {	
			$(".repayment_sim .tab_content").removeClass("cr");
			$(".repayment_sim .tab_menu li").find("a").removeClass("cr");
			$(".repayment_sim .tab_menu li:first-child").find("a").addClass("cr");
			$(".repayment_sim .tab_content:first-child").addClass("cr");
		}
	},
		/* ===================
		お借入シミュレーション　 loanSim
		=================== */
	loanSim = {		
        init: function () {
        	var count = 0;
			$(".loan_sim form").submit(function(){
				var error = false;
				$(this).find("input[type='text']").each(function(){
					if($(this).val() == ""){
						$(this).addClass("error");
						$(this).parents(".input_line").find(".notice").hide();
						$(this).parents(".input_line").find(".error_box").show();
						error = true;
					}
				}); 
				if(!error) {				
					var $step = $(this).parent(".step");
					$step.removeClass("active");
					if(count > 1) {
						$(".step4").addClass("active");	
					}else if(count > 0) {
						$(".step3").addClass("active");
					}else {
						$(".step2").addClass("active");											
					}
					count++;
				}
				return false;
			});
		},
		reset:function() {			
			$(".loan_sim .step").removeClass("active");
			$(".loan_sim .step1").addClass("active");
			$(".loan_sim .error").removeClass("error");
			$(".loan_sim .error_box").hide();
			$(".loan_sim .notice").show();
		},
	},		
	/* ===================
		モーダルウィンドウ
		=================== */
	toggleModal = {
        init: function () {
        	var bodyScroll = 0;
            function t(t) {
                var e = -1 != t.indexOf("#")
                        ? t
                        : "#imgCont",
                    o = 100;
                fixHeader.off();
                bodyScroll = $('body').scrollTop();
                $("body").addClass("no_scroll modal_scroll"),
                $(".contents").css({'top':-1*bodyScroll + 'px'}),
                a.removeClass("active").stop().hide(),
                r.find(e).addClass("active").stop().fadeIn(300);
                r.css({
                    width: r.find(e).outerWidth(),
                    "margin-top": o
                }),
                i.stop().fadeIn(300),
                $(".modal_layer").stop(!0, !0).animate({scrollTop: "0px"
                }, 0)
            }
            function e() {
                var t = i.find(".close_btn");
                t.on("click", function (t) {
                    t.preventDefault(),
                    i.stop().fadeOut(240, function () {
                        a.removeClass("active").hide(),
                        a.find(".cont").html(),
                        $("body").removeClass("no_scroll modal_scroll"),
                        $(".contents").css({'top':0}),
                        $('html, body').scrollTop( bodyScroll);
						fixHeader.init();
                    })
                })
            }
            var i = $("body").find(".modal_item "),
                a = $("body").find(".modal_cont"),
                r = $("body").find(".modal_wrap");
            $("body").find(".open_modal").on("click", function (e) {
                e.preventDefault();
                var i = $(this).attr("href");
                t(i)
            }),
            e()
            }
        },
	/* ===================
		クイックアクセスの位置調整
		=================== */
	quickAccessFix = {
		init:function(w,m) {
			if( document.getElementById("quick") != null ) {
				window.setTimeout( function(){
					quickAccessFix.vFix();
					quickAccessFix.hFix(w,m);
	
				}, 50 );
				$(window).on("scroll",function(){
					quickAccessFix.vFix();
				})
				$(window).on("resize",function(){
					quickAccessFix.hFix(w,m);
				})
			}
		},
		vFix:function(){
			var footerTop = $("#footer").offset().top;
			var footerH = $("#footer").height();
			var quickTop = $("#quick").offset().top;
			var quickH = $("#quick").height();
			var quickPos = quickTop + quickH + 30;
			var scrollTop = $(window).scrollTop();
			var widowH = $(window).height();
			if(quickPos >= footerTop) {
				$("#quick").addClass("limit");
				$("#quick").css({bottom : footerH + 30 + "px"});
			}  
			if($("#quick").hasClass("limit")  &&  (scrollTop + widowH <= footerTop)){
				$("#quick").removeClass("limit");
				$("#quick").css({bottom :"30px"});			
			}
		},
		hFix:function(w,m){
			var widowW = $(window).width();
			if(widowW <= w + m) {
				$("#quick").css({right :(widowW - w) +"px"});							
			}else {
				$("#quick").css({right :m +"px"});				
			}
		}
	},
	/* ===================
		別ウィンドウ
		=================== */
	newWindow = {
		init:function(w,h) {
			$("a.new_window").click(
				function(){
					var target = $(this).attr("href");
					window.open(target,"newWindow","width="+w+",height="+h+",scrollbars=1,resizable=1,status=1");
					return false;
				});
		 }
	},	
$(document).ready(init);
}(window, document, $),function () {}.call(this);