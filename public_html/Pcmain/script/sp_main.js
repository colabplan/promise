window.onpageshow = function() {
	$('iframe').each(function() {
		this.contentWindow.location.reload(true);
		var frameW = $(window).width();
		if($(this).hasClass("sec_child")) {
			frameW -= 30;
		}
		$(this).attr('width', frameW);
		$(this).load(function(){
			$(this).addClass("loaded");
		});
	});
};
!function(window, document, $){
    "use strict";
	 function init() {

$('#date_list_slick').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  asNavFor: '.history_contents',
  focusOnSelect: true,
  infinite:false,
  prevArrow : '<a href="javascript:void(0);" class="navigation prev icon"><span>&#xe918;</span></a>',
  nextArrow : '<a href="javascript:void(0);" class="navigation next icon"><span>&#xe91a;</span></a>',
});
 $('.history_contents').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '#date_list_slick',
  infinite:false,
  prevArrow : '<a href="javascript:void(0);" class="navigation prev icon"><span>&#xe918;</span></a>',
  nextArrow : '<a href="javascript:void(0);" class="navigation next icon"><span>&#xe91a;</span></a>'

});
$('.history_contents').on('afterChange', function(event, slick, currentSlide, nextSlide){
	$('#date_list_slick .item').removeClass("slick-current");
	$('#date_list_slick .item').eq(currentSlide).addClass("slick-current");
});

		

			$(window).load(function(){
				$(".contents").addClass('show');			
				$('[class^=match_h]').matchHeight();	
				var slickSettings =	{
						slidesToShow: 1,
					  slidesToScroll: 1,
					  initialSlide : 0,
					  autoplay: true,
					  autoplaySpeed: 5000,
					  speed : 200,
					  useCSS : false,
					  dots: true,
					  draggable : true,
					  prevArrow : '<a href="javascript:void(0);" class="navigation prev icon"><span>&#xe918;</span></a>',
					  nextArrow : '<a href="javascript:void(0);" class="navigation next icon"><span>&#xe91a;</span></a>'
					 }
				
				$('.carousel .list').each(function(){
					if($(this).hasClass("random")) {
						//slickSettings.initialSlide = Math.floor( Math.random() * $(this).find("li.item").length );						
					    var arr = [];
					    $(this).find("li.item").each(function() {
					        arr.push($(this).html());
					    });
					    arr.sort(function() {
					        return Math.random() - Math.random();
					    });
					    $(this).empty();
					    for(var i=0; i < arr.length; i++) {
					        $(this).append('<li class="item">' + arr[i] + '</li>');
					    }
					}else {
						//slickSettings.initialSlide = 0;
					}
					$(this).slick(slickSettings);
				});
				$(document).on("change",".fileinput",function() {
					var ID = $(this).attr("id");
					var fileName = document.getElementById(ID).value;
					fileName=fileName.replace(/C:\\fakepath\\/g, "" ); 
					console.log(fileName);
					$('.filewrap[rel="'+ID+'"]' ).find(".filename").html(fileName);
				});
				$(".filedelete").click(function() {
					var ID = $(this).attr("rel");
					$('.filewrap[rel="'+ID+'"]' ).find(".filename").html("&nbsp;");
					$("#"+ID).replaceWith('<input type="file" name="'+ID+'" class="fileinput" id="'+ID+'">');
				});
				if(document.getElementById("all") != null) {
					$("#main_menu").height($(window).height() - $("#header").height());
				}
			});
			toggleMenu.init();
			fixHeader.init();
			smScroll.init('scroll','#header',69,500);
			accd.init();
			tab.init();
			toggleModal.init();
			process.init();
			loanPlan.init();
	 }	
	 var toggleMenu = {},
			fixHeader = {},
			smScroll = {},
			bnrCarousel = {},
			accd = {},
			tab = {},
			toggleModal = {},
			process = {},
			loanPlan = {};


	/* ===================
		グロナビ
		=================== */
	toggleMenu = {
		init:function() {
			var w = $(window).width();
			var s = 0;
			$(".toggle_menu").click(
				function(){
					var target= $(this).attr("rel");
					if(document.getElementById("all") != null) {
						$("#main_menu").css({
							height : $(window).height() - $("#header").height() +'px',
							});
					}

					//スクロール位置固定					
					if($(".all_wrapper").hasClass("fixed")) {
						if($(this).hasClass("close_menu")) {
							$(".all_wrapper").removeClass("fixed").css({top: 0,width : 'auto'});	
							$(window).scrollTop(s);
							fixHeader.init();
						}
					}else{
						fixHeader.off();
						w = $(window).width();
						s = $(window).scrollTop();
						$(".all_wrapper").addClass("fixed").css({top:-1*s + 'px' , width : w + 'px'});	
					}
					
					//クリックされたボタン以外のメニューを閉じる
					$(".toggle_menu").each(function(){
						var eTarget = $(this).attr("rel");
						if( eTarget != target) {
							$(this).removeClass("close_menu");
							$('#' + eTarget).hide(250);
						} else {
							$(this).toggleClass("close_menu");
						}				
					});
					//メニュー開閉
					$('#' + target).slideToggle(250);
				}
			);
		 }
	},
		/* ===================
		メニュー固定
		=================== */	
	fixHeader = {
		init:function() {
			if(!$("body").hasClass("nofix")) {
			    $(window).on('load scroll', fixHeader.fixed);	
			 }
		 },
		 off:function() {
			$(window).off('load scroll' ,  fixHeader.fixed);			 
		 },
		 fixed:function() {
		    	var h = $('#header').height();
		    	var s = $(window).scrollTop() ;
		        if (s > h) {
		            $('#header').addClass('fixed');
		        } else {
		            $('#header').removeClass('fixed');
		        }
		 }		 
	},		
		/* ===================
		スムーススクロール smScroll
		=================== */
	smScroll= {
		init:function(cls ,targetEle,  fix ,duration) {
		 $('a.'+ cls + '[href^=#]').click(function(){
		    var target;
		    target = $( $(this).attr('href') );
		    if (target.length == 0) {
		      target = $(targetEle);
		    }
		    $('html, body').animate({scrollTop: target.offset().top - fix},duration);
		    	return false;
			});
		 }
	},
	
	/* ===================
	バナーカルーセル bnrCarousel
	=================== */
	bnrCarousel= {
		init:function() {
			var $carousel,$carouselWrap,$carouselList,$carouselEle,$carouselCnt,$carouselNav,$carouselNavWrap,wrapWidth,wrapHeight,coulmn,cntItem,cntNum,cntHtml;
			$carousel = $(".carousel");
			$carouselWrap = $(".carousel .wrap");
			$carouselList = $(".carousel .list");
			$carouselEle = $(".carousel .item");
			$carouselNavWrap = $(".carousel .navigation_wrap");
			$carouselNav = $(".carousel .navigation");
			$carouselCnt = $(".carousel .control");
			cntHtml = "";
			cntItem = '<a href="javascript:void(0);">●</a>';
			wrapWidth = 0;
			wrapHeight= 0;
			cntNum = $carouselEle.length;
			$carouselEle.width($(window).width());
			
			setTimeout(function(){
				$carouselEle.each(function(){
					wrapWidth = $(this).outerWidth(true);
					wrapHeight = $(this).outerHeight(true);
					cntHtml += cntItem;		
				});
				$carouselEle.eq(0).addClass("cr");
				$carouselCnt[0].innerHTML = cntHtml;	
				$carousel.addClass('active');
				$carouselList.width(wrapWidth*cntNum);
				$carouselList.height(wrapHeight);
				$carouselWrap.height(wrapHeight);	
				$carouselCnt.find("a:first").addClass("cr");					
				$carouselNav.click(
					function(){
							var crIndex = $carouselCnt.find("a").index( $carouselCnt.find(".cr"));
							var gotoIndex;
							var direction;
							if($(this).hasClass("prev")) {
								gotoIndex = crIndex  > 0 ? crIndex - 1  : cntNum -1;
								direction = -1;
							}
							if($(this).hasClass("next")) {
								gotoIndex = crIndex  < cntNum - 1 ? crIndex +1  :  0;
								direction = 1;
							}
							bnrCarousel.goTo(gotoIndex,direction,wrapWidth,$carouselEle,$carouselList,$carouselCnt);
					}				
				);
				$carouselCnt.find("a").click(
					function(){
						var crIndex = $carouselCnt.find("a").index( $carouselCnt.find(".cr"));
						var gotoIndex = $carouselCnt.find("a").index( $(this));
						var direction = gotoIndex > crIndex ? 1 : -1;
						bnrCarousel.goTo(gotoIndex,direction,wrapWidth,$carouselEle,$carouselList,$carouselCnt);
					}				
				);
			},100);			
		 },
		 goTo:function(gotoIndex,direction,wrapWidth,$carouselEle,$carouselList,$carouselCnt) {		 	
				$carouselEle.eq(gotoIndex).css("left", direction*wrapWidth + "px");		
				$carouselList.animate({"left" : -1*direction*wrapWidth  +"px"},function(){
					$carouselCnt.find("a.cr").removeClass("cr");
					$carouselCnt.find("a:nth-child("+(gotoIndex + 1)+")").addClass("cr");
					$carouselEle.removeClass("cr").css("left", "-9999px");
					$carouselEle.eq(gotoIndex).css("left",0).addClass("cr");
					$carouselList.css("left",0);
				});		 
		 }
	},
		/* ===================
		アコーディオン accd
		=================== */
	accd= {
		init:function() {
			$(".accd_btn").each(function(){
				if(!$(this).hasClass("open")) {
					var rel = $(this).attr("rel");
					$('.accd_contents[rel$="'+rel+'"]').hide();
				}
			});
			$(".accd_btn").click(
				function(){
					var $btn =  $(this);
					var rel = $(this).attr("rel");
					if($btn.hasClass("hide")) {
						$('.accd_contents[rel$="'+rel+'"]').fadeIn(500);
						$btn.fadeOut(500);
					} else {
						$btn.toggleClass("open");
						$('.accd_contents[rel$="'+rel+'"]').slideToggle(500,function(){
							if($btn.hasClass("open")) {
								var txt =$btn.attr("data-closed");
								$btn.find("span.change").text(txt);
							}else{
								var txt =$btn.attr("data-opened");
								$btn.find("span.change").text(txt);		
							}
						});
					}
				}
			);
		 }
	},
		/* ===================
		タブ tab
		=================== */
	tab = {
		init:function() {
			$(".tab_content").not(".cr").hide();	
			$(".tab_menu a").click(
				function(){
					$(this).parents(".tab_menu").find("a").removeClass("cr");
					var relTab = $(this).parents(".tab_menu").attr("rel");
					var relContent  = $(this).attr("rel");
					$("#"+relTab + " .tab_content").hide().removeClass("cr");
					$(this).addClass("cr");
					$("#"+relContent).fadeIn().addClass("cr");
				}
			);
		 }
	},	
	/* ===================
		モーダルウィンドウ
		=================== */
	toggleModal = {
        init: function () {
        	var bodyScroll = 0;
            function t(t) {
                var e = -1 != t.indexOf("#")
                        ? t
                        : "#imgCont",
                    o = 100;
                bodyScroll = $('body').scrollTop();
                $("body").addClass("no_scroll modal_scroll"),
                $("#all_wrapper").css({'top':-1*bodyScroll + 'px'}),
                a.removeClass("active").stop().hide(),
                r.find(e).addClass("active").stop().fadeIn(300);
                r.css({
                    width: r.find(e).outerWidth(),
                    "margin-top": o
                }),
                i.stop().fadeIn(300),
                $(".modal_layer").stop(!0, !0).animate({scrollTop: "0px"
                }, 0)
            }
            function e() {
                var t = i.find(".close_btn");
                t.on("click", function (t) {
                    t.preventDefault(),
                    i.stop().fadeOut(240, function () {
                        a.removeClass("active").hide(),
                        a.find(".cont").html(),
                        $("body").removeClass("no_scroll modal_scroll"),
                        $("#all_wrapper").css({'top':0}),
                        $('html, body').scrollTop( bodyScroll);
                    })
                })
            }
            var i = $("body").find(".modal_item "),
                a = $("body").find(".modal_cont"),
                r = $("body").find(".modal_wrap");
            $("body").find(".open_modal").on("click", function (e) {
                e.preventDefault();
                var i = $(this).attr("href");
                t(i)
            }),
            e()
            }
        },
		/* ===================
		process
		=================== */
	process = {
		init:function() {
			$(".process_btn").click(
				function(){
					var listW = $(this).parents(".process_list").width();
					var listL = $(this).parents(".process_list").offset().left;
					var btnL = $(this).offset().left;
					$(this).next(".tab_content").width(listW).css("left" , (btnL - listL)*-1+"px");

					var contentH =  $(this).next(".tab_content").height();					
					$(this).parents(".process_list").css("padding-bottom",(contentH) + "px");

					if($(this).hasClass("next")) {
						var target = $(this).data("target");
						var targetList = $(this).data("next");
						$(this).parents(".flow_item").addClass("active");
						var $flowItem = $(this).parents(".flow_item").nextAll(".flow_item");
						$flowItem.removeClass("active");
						$flowItem.find(".active").removeClass("active");
						$flowItem.find(".tab_content").removeClass("cr").hide();
						$flowItem.find(".process_btn").removeClass("cr");					
						$flowItem.find(".process_list").css("padding-bottom","0px");						
						$(target + " .title_wrap").addClass("active");
						$(target + " .process_wrap").each(function(){
							if($(this).is(targetList)) {
								$(this).addClass("active");
							}else {
								$(this).removeClass("active");
							}
						})
					}
				}
			);
		 }
	},	
		/* ===================
		お借入プラン診断　 loanPlan
		=================== */
	loanPlan = {
        init: function () {
			var loanPlanSelected = new Array();
			
			$(".loan_plan a.return").click(function(){
				var gotostep = $(this).data("goto");
				var reset = $(this).data("reset");
				if( reset == 1) {
					loanPlan.reset();
				}else {
					$(this).siblings().removeClass("active");
					$(this).addClass("active");
					$(this).parents(".step").removeClass("active");
				}
				$(".step" + gotostep).addClass("active");
			});
			$(".loan_plan a.select").click(function(){
				var step = $(this).data("step");
				var select = $(this).data("select");
				var gotostep = $(this).data("goto");
				loanPlanSelected[step -1] = select;
				$(this).siblings().removeClass("active");
				$(this).addClass("active");
				$(this).parents(".step").removeClass("active");
				$(".step" + gotostep).addClass("active");
			});
			$( '.loan_plan a.select , .loan_plan a.return' )
			  .on( 'touchstart', function(){
			    $(this).addClass("active");
			}).on( 'touchend', function(){
			    $(this).addClass("active");
			});
		},
		reset:function() {
			$(".loan_plan .active").removeClass("active");
			$(".step1").addClass("active");
			loanPlan.loanPlanSelected= [];				
		}
	},
$(document).ready(init);
}(window, document, $),function () {}.call(this);